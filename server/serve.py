import json
import pymysql
from flask import Flask
from flask_cors import CORS, cross_origin

SERVER_HOST = 'localhost'
DB_USER = 'root'
DB_PASSWORD = '123456'
SCHEMA_NAME = 'app_db'


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
@cross_origin()
def get_index():
    return 'Server has been started successfully'

@app.route('/musician/artist_by_birth_year/<int:birth_year>')
@cross_origin()
def get_musician_artist_by_birth_year(birth_year):
    query = f'''
select artists.artist_name
from artists, artist_type
where birth_date = {birth_year}
and artists.artist_type = artist_type.id_artist_type
and artist_type.artist_type_name = 'Person'
limit 20
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            data.append(res)
    except Exception as e:
        print('error /musician/artist_by_birth_year:', e)
        data = []

    return json.dumps(data)


@app.route('/musician/average_songs_per_artist')
@cross_origin()
def get_musician_average_songs_per_artist():
    query = f'''
select avg(b.num_songs) as avg_songs_number
from (select COUNT(*) as num_songs
from artists, recordings
where recordings.artist_id = artists.id_artist
group by artists.artist_name) as b
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchone()
        cursor.close()
        data = str(float(raw_results[0]))
    except Exception as e:
        print('error /musician/artist_by_birth_year:', e)
        data = 0

    return data


@app.route('/musician/average_songs_per_artist_by_birth_year/', defaults={'birth_year': ''})
@app.route('/musician/average_songs_per_artist_by_birth_year/<int:birth_year>')
@cross_origin()
def get_musician_average_songs_per_artist_by_birth_year(birth_year):
    query = f'''
select avg(b.num_songs) as avg_songs_number
from (select COUNT(*) as num_songs
from artists, recordings
where recordings.artist_id = artists.id_artist
and artists.birth_date <= ({birth_year} + 2) and artists.birth_date >= ({birth_year} - 3)
group by artists.artist_name) as b
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchone()
        cursor.close()
        data = str(float(raw_results[0]))
    except Exception as e:
        print('error /musician/artist_by_birth_year:', e)
        data = '0'

    return data


@app.route('/musician/most_played_place_type')
@cross_origin()
def get_musician_most_played_place_type():
    query = f'''
select distinct c.area_type_name
from  ( select b.type_area,b.counter,b.area_type_name, max(b.counter) over () as max_counter
	from ( select area.type_area,COUNT(*) as counter, area_type.name as area_type_name
		from places, area, area_type
		where places.area = area.id_area
        and area.type_area = area_type.id_area_type
        group by type_area) as b
                 ) as c
where c.counter = c.max_counter
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchone()
        cursor.close()
        data = str(raw_results[0])
    except Exception as e:
        print('error /musician/most_played_place_type:', e)
        data = 'None'

    return data


@app.route('/musician/ages_distribution')
@cross_origin()
def get_musician_ages_distribution():
    query = '''
select birth_date, 
(count(*) * 100) / (select count(*) from app.artists where birth_date is not null) as percent_of_artists
from app.artists
group by birth_date
having birth_date is not null
order by percent_of_artists desc
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, 'app')
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        sum_tail = 0
        index = 0
        for res in raw_results:
            if index < len(raw_results):
                name_val_dict = {
                    'name': str(res[0]),
                    'value': float(res[1])
                }
                data.append(name_val_dict)
            else:
                sum_tail += float(res[1])
            index += 1
    
    except Exception as e:
        print('error /musician/ages_distribution:', e)
        data = []
    # data.append({'name': 'other', 'value': sum_tail})

    return json.dumps(data)


@app.route('/musician/years_alive_distribution')
@cross_origin()
def get_musician_years_alive_distribution():
    query = '''
select death_date - birth_date, count(*) as artist_count
from artists
where death_date > birth_date
group by death_date - birth_date
order by artist_count desc
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, 'app')
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            name_val_dict = {
                'name': str(res[0]),
                'value': float(res[1])
            }
            data.append(name_val_dict)
    except Exception as e:
        print('error /musician/years_alive_distribution:', e)
        data = []

    return json.dumps(data)

@app.route('/consumer/artist_search/', defaults={'artist': ''})
@app.route('/consumer/artist_search/<artist>')
@cross_origin()
def get_consumer_artist_search(artist):
    query = f'''
select artist_name, gender,birth_date, death_date, recording_name, artist_type.artist_type_name
from recordings, artists, artist_type
where recordings.artist_id = artists.id_artist
and artists.artist_type = artist_type.id_artist_type
and artist_name like '%{artist}%'
limit 20
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            data.append(res)
    except Exception as e:
        print('error /consumer/artist_search/:', e)
        data = []

    return json.dumps(data)


@app.route('/consumer/instrument_search/', defaults={'instrument': ''})
@app.route('/consumer/instrument_search/<instrument>')
@cross_origin()
def get_consumer_instrument_search(instrument):
    query = f'''
select instrument_name,short_explanation
from instruments
where instrument_name like '%{instrument}%'
and short_explanation is not null
limit 20
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            data.append(res)
    except Exception as e:
        print('error /consumer/instrument_search/:', e)
        data = []

    return json.dumps(data)


@app.route('/consumer/album_search/', defaults={'album': ''})
@app.route('/consumer/album_search/<album>')
@cross_origin()
def get_consumer_album_search(album):
    query = f'''
select release_name
from releases
where release_name LIKE '%{album}%'
limit 20
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            data.append(res)
    except Exception as e:
        print('error /consumer/album_search/:', e)
        data = []

    return json.dumps(data)


@app.route('/consumer/works_search/', defaults={'work_type': ''})
@app.route('/consumer/works_search/<work_type>')
@cross_origin()
def get_consumer_works_search(work_type):
    query = f'''
select  works.work_name,  work_type.work_type_name
from works, work_type
where works.work_type = work_type.id_work_type
AND work_type.work_type_name like '%{work_type}%'
limit 20
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            data.append(res)
    except Exception as e:
        print('error /consumer/works_search/:', e)
        data = []

    return json.dumps(data)


@app.route('/consumer/common_artist_types')
@cross_origin()
def get_consumer_common_artist_types():
    query = '''
select COUNT(*), artist_type.artist_type_name
from recordings, artists, artist_type
where recordings.artist_id = artists.id_artist
and artists.artist_type = artist_type.id_artist_type
group by artist_type.id_artist_type
order by COUNT(*) desc limit 3
    '''
    try:
        db = pymysql.connect(SERVER_HOST, DB_USER, DB_PASSWORD, SCHEMA_NAME)
        cursor = db.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        cursor.close()
        data = []
        for res in raw_results:
            name_val_dict = {
                'name': str(res[1]),
                'value': float(res[0])
            }
            data.append(name_val_dict)
    except Exception as e:
        print('error /consumer/common_artist_types:', e)
        data = []

    return json.dumps(data)
