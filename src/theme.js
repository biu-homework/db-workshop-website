import { red, grey, blueGrey, lightGreen, orange, deepOrange, deepPurple, purple } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      main: deepOrange[200],
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: deepOrange[200],
    },
  },
});

export default theme;
