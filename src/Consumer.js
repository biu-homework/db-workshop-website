import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  button: {
    width: '100%',
    minHeight: 60,
    fontSize: 25,
  },
  gridRoot: {
    flexGrow: 1,
    marginBottom: theme.spacing(6),
    marginTop: theme.spacing(6),
  },
  gridItem: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  }
}));


export default function Consumer() {
  const classes = useStyles();

  return (
    <Container className={classes.root} maxWidth="xl">
      <Typography variant="h4" component="h1" gutterBottom align="center">
        What would you like to know about?
      </Typography>

      <div className={classes.gridRoot}>
        <Grid container spacing={1}>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/consumer/artist_info">
              search for an artist
            </Button>
          </Grid>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/consumer/instrument">
              search for an instrument
            </Button>
          </Grid>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/consumer/album_info">
              search for an album
            </Button>
          </Grid>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/consumer/works">
              search for musical works
            </Button>
          </Grid>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/consumer/common_artist_types">
              common artist types
            </Button>
          </Grid>
        </Grid>
      </div>

    </Container >
  );
}
