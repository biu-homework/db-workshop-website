import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  paper: {
    padding: 15,
    width: '100%',
  },
  box: {
    width: '100%',
  }
}));

export function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link href="/">Musicallies</Link>
      {' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function PageTemplate(props) {
  const classes = useStyles();

  return (
    <Container maxWidth="xl" className={classes.root}>
      <Box my={4} className={classes.box} >
        <Paper className={classes.paper}>
          {props.pageContent}
          <Copyright />
        </Paper>
      </Box>
    </Container >
  );
}
