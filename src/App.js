import React from 'react';
import Home from './Home';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Musician from './Musician';
import Consumer from './Consumer';
import PageTemplate from './PageTemplate';
import TableQuery from './TableQuery';
import CommonArtistTypes from './CommonArtistTypes';
import FetchAndDisplaySingleValue from './FetchAndDisplaySingleValue';
import FetchAndDisplaySingleValueWithInput from './FetchAndDisplaySingleValueWithInput';

const serverUrl = 'http://localhost:5000'

export default function App() {
  document.title = 'Musicallies';
  
  return (
    <Router>
      <Switch>
      <Route path="/musician/average_songs_per_artist_by_birth_year">
          <PageTemplate pageContent={<FetchAndDisplaySingleValueWithInput
           url={`${serverUrl}/musician/average_songs_per_artist_by_birth_year`}
           title="Average Songs Per Artist"
           searchBarLabel="Enter your birth year" />} />} />
        </Route>
        <Route path="/musician/artist_by_birth_year">
          <PageTemplate pageContent={<TableQuery
            dataUrl={`${serverUrl}/musician/artist_by_birth_year`}
            title="Artists By Birth Year"
            searchBarLabel="Enter your birth year"
            rowTitles={['Artist Name']}
          />} />
        </Route>
        <Route path="/musician/average_songs_per_artist">
          <PageTemplate pageContent={<FetchAndDisplaySingleValue
           url={`${serverUrl}/musician/average_songs_per_artist`}
           title="Average Songs Per Artist" />} />
        </Route>
        <Route path="/musician/most_played_place_type">
          <PageTemplate pageContent={<FetchAndDisplaySingleValue
           url={`${serverUrl}/musician/most_played_place_type`}
           title="Most Played Place Type" />} />
        </Route>
        <Route path="/musician">
          <PageTemplate pageContent={<Musician />} />
        </Route>
        <Route path="/consumer/artist_info">
          <PageTemplate pageContent={<TableQuery
            dataUrl={`${serverUrl}/consumer/artist_search`}
            title="Artist Info"
            searchBarLabel="Search for an artist"
            rowTitles={['Artist Name', 'Gender', 'Birth Year', 'Death Year', 'Recording Name', 'Artist Type']}
          />} />
        </Route>
        <Route path="/consumer/instrument">
          <PageTemplate pageContent={<TableQuery
            dataUrl={`${serverUrl}/consumer/instrument_search`}
            title="Instrument Search"
            searchBarLabel="Search for an instrument"
            rowTitles={['Instrument Name', 'Description']}
          />} />
        </Route>
        <Route path="/consumer/album_info">
          <PageTemplate pageContent={<TableQuery
            dataUrl={`${serverUrl}/consumer/album_search`}
            title="Album Info"
            searchBarLabel="Search for an album"
            rowTitles={['Album Name']}
          />} />
        </Route>
        <Route path="/consumer/works">
          <PageTemplate pageContent={<TableQuery
            dataUrl={`${serverUrl}/consumer/works_search`}
            title="Musical Works Search"
            searchBarLabel="Search for a musical work by type"
            rowTitles={['Work Name', 'Type']}
          />} />
        </Route>
        <Route path="/consumer/common_artist_types">
          <PageTemplate pageContent={<CommonArtistTypes serverUrl={serverUrl} />} />
        </Route>
        <Route path="/consumer">
          <PageTemplate pageContent={<Consumer />} />
        </Route>
        <Route path="/">
          <PageTemplate pageContent={<Home />} />
        </Route>
      </Switch>
    </Router>
  );
}
