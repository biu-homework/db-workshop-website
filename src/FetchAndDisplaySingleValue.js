import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import { Alert } from './PageTemplate';
import Snackbar from '@material-ui/core/Snackbar';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  card: {
    padding: theme.spacing(1),
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(10),
    background: '#f3e5f5',
  }
}));

export default function FetchAndDisplaySingleValue(props) {
  const [data, setData] = useState('');
  const [openAlert, setOpenAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const classes = useStyles();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenAlert(false);
  };

  useEffect(() => {
    async function fetchData() {
      fetch(props.url)
        .then((res) => {
          if (!res.ok) {
            setErrorMessage('The server experienced an error while trying to fulfill your request');
            setOpenAlert(true);
          }
          return res;
        })
        .then(res => res.text())
        .then(setData)
        .catch((error) => {
          setErrorMessage('An error occurred while trying to reach the server');
          setOpenAlert(true);
        })
    }
    fetchData();
  }, []);

  return (
    <Container maxWidth="xl" className={classes.root}>
      <Typography variant="h4" component="h1" gutterBottom align="center">
        {props.title}
      </Typography>

      <Card className={classes.card}>
        <Typography variant="h4" component="h1" gutterBottom align="center">
          {`${data}`}
        </Typography>
      </Card>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorMessage}
        </Alert>
      </Snackbar>
    </Container >
  );
}
