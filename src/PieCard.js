import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import {
  PieChart, Pie, Legend, Tooltip,
} from 'recharts';
import { Typography } from '@material-ui/core';
import { Alert } from './PageTemplate';
import Snackbar from '@material-ui/core/Snackbar';

const useStyles = makeStyles(theme => ({
  card: {
    padding: theme.spacing(1),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  }
}));

export default function PieCard(props) {
  const [data, setData] = useState([{ name: '0000', value: 1 }]);
  const [openAlert, setOpenAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const classes = useStyles();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenAlert(false);
  };

  useEffect(() => {
    async function fetchData() {
      fetch(props.url)
        .then((res) => {
          if (!res.ok) {
            setErrorMessage('The server experienced an error while trying to fulfill your request');
            setOpenAlert(true);
          }
          return res;
        })
        .then(res => res.json())
        .then(setData)
        .catch((error) => {
          setErrorMessage('An error occurred while trying to reach the server');
          setOpenAlert(true);
        })
    }
    fetchData();
  }, []);

  return (
    <div>
      <Card className={classes.card} style={{ width: props.width + 25, }} variant="outlined">
        <Typography align="center">{props.title}</Typography>
        <PieChart width={props.width + 5} height={props.height + 5}>
          <Pie dataKey="value" isAnimationActive={true} data={data}
            cx={props.width / 2} cy={props.height / 2} outerRadius={props.width / 2} fill={props.fill} label={props.label} />
          <Tooltip />
        </PieChart>
      </Card>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorMessage}
        </Alert>
      </Snackbar>
    </div>
  );
}
