import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import { Alert } from './PageTemplate';
import Snackbar from '@material-ui/core/Snackbar';
import { TextField } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  card: {
    padding: theme.spacing(1),
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(10),
    background: '#f3e5f5',
  },
  searchBar: {
    marginTop: theme.spacing(3),
  }
}));

export default function FetchAndDisplaySingleValueWithInput(props) {
  const [data, setData] = useState('');
  const [openAlert, setOpenAlert] = useState(false);
  const [querySearchVal, setQuerySearchVal] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const classes = useStyles();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenAlert(false);
  };

  function onQuerySearchChanged(event) {
    console.log('onQuerySearchChanged:', event.target.value)
    setQuerySearchVal(event.target.value);
  }

  function onQuerySearchEnter(ev) {
    if (ev.key === 'Enter') {
      fetch(`${props.url}/${querySearchVal}`)
        .then((res) => {
          if (!res.ok) {
            setErrorMessage('The server experienced an error while trying to fulfill your request');
            setOpenAlert(true);
          }
          return res;
        })
        .then(res => res.text())
        .then(setData)
        .catch((error) => {
          setErrorMessage('An error occurred while trying to reach the server');
          setOpenAlert(true);
        });

      console.log('ENTER!!!', querySearchVal);
      ev.preventDefault();
    }
  }

  return (
    <Container maxWidth="xl" className={classes.root}>
      <Typography variant="h4" component="h1" gutterBottom align="center">
        {props.title}
      </Typography>
      <TextField className={classes.searchBar} id="query-search" label={props.searchBarLabel}
        variant="outlined" onChange={onQuerySearchChanged} onKeyPress={onQuerySearchEnter} />
      <Card className={classes.card}>
        <Typography variant="h4" component="h1" gutterBottom align="center">
          {`${data}`}
        </Typography>
      </Card>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorMessage}
        </Alert>
      </Snackbar>
    </Container >
  );
}
