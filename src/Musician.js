import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  button: {
    width: '100%',
    minHeight: 60,
    fontSize: 25,
  },
  gridRoot: {
    flexGrow: 1,
    marginBottom: theme.spacing(6),
    marginTop: theme.spacing(6),
  },
  gridItem: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  }
}));


export default function Musician() {
  const classes = useStyles();

  return (
    <Container className={classes.root} maxWidth="xl">
      <Typography variant="h4" component="h1" gutterBottom align="center">
        What would you like to know about?
      </Typography>

      <div className={classes.gridRoot}>
        <Grid container spacing={1}>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/musician/most_played_place_type">
              venue types with the most shows played
            </Button>
          </Grid>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/musician/artist_by_birth_year">
              find artists your age
            </Button>
          </Grid>
          <Grid className={classes.gridItem} item xs={12}>
            <Button className={classes.button} variant="outlined" color="primary" href="/musician/average_songs_per_artist_by_birth_year">
              average amount of songs for an artist
            </Button>
          </Grid>
        </Grid>
      </div>

    </Container >
  );
}
