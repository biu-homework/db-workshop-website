import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import PieCard from './PieCard';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  gridRoot: {
    flexGrow: 1,
    marginBottom: theme.spacing(2),
  },
}));

export default function CommonArtistTypes(props) {
  const classes = useStyles();

  return (
    <Container maxWidth="xl" className={classes.root}>
      <Typography variant="h4" component="h1" gutterBottom align="center">
        Most Common Artist Types
      </Typography>
      <PieCard width={400} height={400} url={`${props.serverUrl}/consumer/common_artist_types`}
        fill="#82ca9d" title="Common Artist Types" />
    </Container >
  );
}
