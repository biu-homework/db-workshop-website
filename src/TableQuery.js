import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Alert } from './PageTemplate';
import Snackbar from '@material-ui/core/Snackbar';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  button: {
    margin: theme.spacing(4),
  },
  table: {
    width: '99%',
    margin: theme.spacing(1),
  },
  tableContainer: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    width: '99%',
  },
  searchBar: {
    width: 280,
  }
}));


export default function TableQuery(props) {
  const [querySearchVal, setQuerySearchVal] = useState('');
  const [results, setResults] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const classes = useStyles();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenAlert(false);
  };

  function onQuerySearchChanged(event) {
    console.log('onQuerySearchChanged:', event.target.value)
    setQuerySearchVal(event.target.value);
  }

  function onQuerySearchEnter(ev) {
    if (ev.key === 'Enter') {
      setResults([]);
      fetch(`${props.dataUrl}/${querySearchVal}`)
        .then((res) => {
          if (!res.ok) {
            setErrorMessage('The server experienced an error while trying to fulfill your request');
            setOpenAlert(true);
          }
          return res;
        })
        .then(res => res.json())
        .then(setResults)
        .catch((error) => {
          setErrorMessage('An error occurred while trying to reach the server');
          setOpenAlert(true);
        });

      console.log('ENTER!!!', querySearchVal);
      ev.preventDefault();
    }
  }

  return (
    <Container className={classes.root} maxWidth="xl">
      <Typography variant="h4" component="h1" gutterBottom align="center">
        {props.title}
      </Typography>
      <TextField className={classes.searchBar} id="query-search" label={props.searchBarLabel}
        variant="outlined" onChange={onQuerySearchChanged} onKeyPress={onQuerySearchEnter} />
      <TableContainer component={Paper} className={classes.tableContainer}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              {props.rowTitles.map((rowTitle, idx) => {
                return <TableCell align={idx == 0 ? "left" : "right"}>{rowTitle}</TableCell>;
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {results.map(result => (
              <TableRow key={result[0]}>
                {result.map((entry, idx) => {
                  if (idx == 0) {
                    return (<TableCell component="th" scope="row">
                      {entry}
                    </TableCell>);
                  }
                  else {
                    return (<TableCell align="right">{entry}</TableCell>);
                  }
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorMessage}
        </Alert>
      </Snackbar>
    </Container >
  );
}
