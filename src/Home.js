import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Link } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(4),
  },
  box: {
    width: '100%',
  }
}));

export default function Home() {
  const classes = useStyles();

  return (
    <Container>
      <Typography variant="h4" component="h1" gutterBottom align="center">
        Are you a musician or a consumer?
          </Typography>
      <Box align="center" my={5} px={20} className={classes.box}>
        <Button variant="contained" color="primary" className={classes.button} href="/musician">Musician</Button>
        <Button variant="contained" color="primary" className={classes.button} href="/consumer">Consumer</Button>
      </Box>
    </Container>
  );
}
