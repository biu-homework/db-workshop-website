This is Musicallies project for the DB workshop

## Installation

Follow these instructions in order to correctly install and run the app

### Prerequisites

- git (optional - can just download zip if you're not developing the project)
- [node](https://nodejs.org/en/) - download the current LTS version
- [python 3.6+](https://www.python.org/downloads/)

### Get the code

You can either clone or download the zip for: <br />
`git clone https://gitlab.com/biu-homework/db-workshop-website.git`<br />
Download the zip for https://gitlab.com/biu-homework/db-workshop-setup

### Install dependencies

`cd db-workshop-website`<br />
`cd server`<br />
create a python virtual environment<br />
`python3 -m venv server`<br />
`source server/bin/activate`<br />
or on Windows Powershell:<br />
`Set-ExecutionPolicy RemoteSigned`<br />
`.\server\Scripts\activate`

`pip install -r requirements.txt`<br />
exit the virtual environment<br />
`deactivate`<br />
now for the npm dependencies<br />
`cd ../`<br />
`npm install`

### Verify it works

Verify frontend<br />
`npm start`<br />
Your browser should open to the page, navigate a little everything the queries themselves should be working

Verify backend<br />
`cd ../server`<br />
`export FLASK_APP=serve.py`<br />
`flask run`<br />
It should print that it's listening on port 5000<br />
Navigate with your browser to http://localhost:5000 and you should see an OK message

## Load the DB with data

TODO: Add missing